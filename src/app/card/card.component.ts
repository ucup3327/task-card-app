import { Component, Input, OnInit } from '@angular/core';
import { Task } from '../model/task';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() task: Task;

  statusToggle() {
    this.task.completed = !this.task.completed;
  }

  constructor() { }

  ngOnInit() {
  }

}
